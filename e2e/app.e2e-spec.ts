import { EzcoworkPage } from './app.po';

describe('ezcowork App', function() {
  let page: EzcoworkPage;

  beforeEach(() => {
    page = new EzcoworkPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
