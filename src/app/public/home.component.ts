﻿import {
  Component,
  OnInit,
  OnDestroy
} from "@angular/core";
import { Router } from "@angular/router";

import { UserDatabase } from "../user/user.service";
import { JasoseoWrapperComponent } from "./jasoseoWrapper.component";

declare let AWS: any;
declare let AWSCognito: any;


@Component({
  selector: "jasoseo-home",
  host: {
    class: ""
  },
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.css"]
})
export class HomeComponent implements OnInit, OnDestroy {
  loginFormActivated: boolean = true;

  constructor(private router: Router, private _userDatabase: UserDatabase, private _jasoseoWrapper: JasoseoWrapperComponent) {
    console.log("HomeComponent constructor");
  }

  ngOnInit() {
    this._jasoseoWrapper.isAuthenticated = this._userDatabase.isAuthenticated();
  }

  ngOnDestroy() {

  }

  registrationClicked() {
    this.loginFormActivated = !this.loginFormActivated;
  }

  onNotifyFromLoginComponent(loggedIn: boolean) {
    this._jasoseoWrapper.isAuthenticated = this._userDatabase.isAuthenticated();
  }
}