﻿import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";

import { UserDatabase } from "../user/user.service";


@Component({
  selector: "jasoseo-main",
  host: {
    class: ""
  },
  templateUrl: "./jasoseoWrapper.component.html",
  styleUrls: ["./jasoseoWrapper.component.css"]
})
export class JasoseoWrapperComponent implements OnInit {
  isAuthenticated: any;

  constructor(public router: Router, private _userDatabase: UserDatabase) {
    console.log("WrapperComponent constructor");

  }

  ngOnInit() {
    this.isAuthenticated = this._userDatabase.isAuthenticated();
  }
  onNotifyFromProjectListComponent() {
    this.isAuthenticated = this._userDatabase.isAuthenticated();
  }
}