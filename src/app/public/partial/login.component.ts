﻿import { Component, OnInit, EventEmitter, Output } from "@angular/core";
import { Router } from "@angular/router";

import {
  UserDatabase,
  UserLoginArgument
} from "../../user/user.service";


@Component({
  selector: "login-form",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"]
})
export class LoginComponent implements OnInit {
  @Output() notify: EventEmitter<boolean> = new EventEmitter<boolean>();
  email: string;
  password: string;
  errorMessage: string;

  constructor(private router: Router, private _userDatabase: UserDatabase) {
    console.log("LoginComponent constructor");
  }

  ngOnInit() {

  }

  onLogin() {
    if ((this.email == null) || (this.password == null)) {
      this.errorMessage = "All fields are required";
      return;
    }
    this.errorMessage = null;

    let userLoginArguments = new UserLoginArgument(this.email, this.password);
    this._userDatabase.login(userLoginArguments)
    .then((result) => {
      console.log("success");
      this.notify.emit(true);
    }, (errorResult) => {
      console.log("failed");
      this.notify.emit(false);
    })
    .catch((errorResult) => {
      console.log("throwed");
      this.notify.emit(false);
    });
  }
}