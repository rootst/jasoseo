﻿import { Component, OnInit, Output, EventEmitter } from "@angular/core";
import { Router } from "@angular/router";

import { UserDatabase } from "../../user/user.service";


@Component({
  selector: "jasoseo-header",
  host: {
    class: ""
  },
  templateUrl: "./jasoseoHeader.component.html",
  styleUrls: ["./jasoseoHeader.component.css"]
})
export class JasoseoHeaderComponent implements OnInit {
  currentUser: any;

  constructor(private router: Router, private _userDatabase: UserDatabase) {
    console.log("JasoseoHeaderComponent constructor");
  }

  ngOnInit() {
    this.currentUser = this._userDatabase.getUserPool().getCurrentUser();
  }
}