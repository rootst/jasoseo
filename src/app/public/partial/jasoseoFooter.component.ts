﻿import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";


@Component({
  selector: "jasoseo-footer",
  host: {
    class: ""
  },
  templateUrl: "./jasoseoFooter.component.html",
  styleUrls: ["./jasoseoFooter.component.css"]
})
export class JasoseoFooterComponent implements OnInit {
  constructor(public router: Router) {
    console.log("JasoseoFooterComponent constructor");
  }

  ngOnInit() {

  }
}