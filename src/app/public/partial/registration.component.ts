﻿import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";

import { User } from "../../user/user";
import {
  UserDatabase,
  UserCreateItemArgument
} from "../../user/user.service";


/**
 * This component is responsible for displaying and controlling
 * the registration of the user.
 */
@Component({
  selector: "register-form",
  templateUrl: "./registration.component.html"
})
export class RegistrationComponent {
  registrationUser: User;
  router: Router;
  errorMessage: string;

  constructor(private _userDatabase: UserDatabase, router: Router) {
    this.router = router;
    this.onInit();
  }

  onInit() {
    this.registrationUser = new User();
    this.errorMessage = null;
  }

  onRegister() {
    this.errorMessage = null;

    let userCreateItemArgument = new UserCreateItemArgument(this.registrationUser.email, this.registrationUser.password);
    userCreateItemArgument.addAttribute("email", this.registrationUser.email);
    this._userDatabase.createItem(userCreateItemArgument)
    .then((result) => {
      console.log(result);
    }, (errorResult) => {
      console.log(errorResult);
    })
    .catch((errorResult) => {
      console.log(errorResult);
    });
  }
}