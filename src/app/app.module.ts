import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { HttpModule } from "@angular/http";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { InfiniteScrollModule } from "angular2-infinite-scroll";

import { AppRoute } from "./app.route";

// Pipes
import { CustomPipe, DynamoDBAttributePipe } from "./shared/pipes";

// Components
import { AppComponent } from "./app.component";

import { JasoseoWrapperComponent } from "./public/jasoseoWrapper.component";
import { JasoseoHeaderComponent } from "./public/partial/jasoseoHeader.component";
import { JasoseoFooterComponent } from "./public/partial/jasoseoFooter.component";
import { LoginComponent } from "./public/partial/login.component";
import { RegistrationComponent } from "./public/partial/registration.component";

import { HomeComponent } from "./public/home.component";

import { ProjectListComponent } from "./product/product-list.component";
import { ProjectNewComponent } from "./product/product-new.component";
import { ProjectRetrieveComponent } from "./product/product-retrieve.component";

import { UserRetrieveComponent } from "./user/user-retrieve.component";

// Services
import { AuthenticationGuard } from "./shared/authenticationGuard";
import {
  UserCreateItemArgument,
  UserGetItemArgument,
  UserAuthenticatorEnvironment,
  UserConfirmRegistrationArgument,
  UserResendConfirmRegistrationArgument,
  UserLoginArgument,
  UserForgotPasswordArgument,
  UserConfirmPasswordArgument,
  UserDatabase
} from "./user/user.service";
import {
  ProjectCreateItemArgument,
  ProjectGetItemArgument,
  ProjectAuthenticatorEnvironment,
  ProjectDatabase
} from "./product/product.service";

import {
  JasoseoPlatformEnvironment,
  JasoseoAmazonWebService,
  JasoseoS3Environment,
  JasoseoS3
} from "./shared/jasoseo";


@NgModule({
  declarations: [
    AppComponent,

    CustomPipe,
    DynamoDBAttributePipe,

    JasoseoWrapperComponent,
    JasoseoHeaderComponent,
    JasoseoFooterComponent,

    HomeComponent,
    LoginComponent,
    RegistrationComponent,

    ProjectListComponent,
    ProjectNewComponent,
    ProjectRetrieveComponent,

    UserRetrieveComponent
  ],
  imports: [
    NgbModule.forRoot(),
    BrowserModule,
    FormsModule,
    HttpModule,
    InfiniteScrollModule,

    AppRoute,
  ],
  providers: [
    AuthenticationGuard,

    JasoseoPlatformEnvironment,
    JasoseoAmazonWebService,
    JasoseoS3Environment,
    JasoseoS3,

    UserCreateItemArgument,
    UserGetItemArgument,
    UserAuthenticatorEnvironment,
    UserConfirmRegistrationArgument,
    UserResendConfirmRegistrationArgument,
    UserLoginArgument,
    UserForgotPasswordArgument,
    UserConfirmPasswordArgument,
    UserDatabase,

    ProjectCreateItemArgument,
    ProjectGetItemArgument,
    ProjectAuthenticatorEnvironment,
    ProjectDatabase
  ],
  bootstrap: [AppComponent]
})

export class AppModule { }