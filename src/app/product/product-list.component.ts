import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { DynamoDBPaginationResult } from "@room36/database/mixin/pagination";

import { ProductDatabase } from "./product.service";
import { UserDatabase } from "../user/user.service";


@Component({
  selector: "product-list",
  host: {
    class: ""
  },
  templateUrl: "./product-list.component.html",
  styleUrls: ["./product-list.component.css"]
})
export class ProductListComponent implements OnInit {
  items:Array<any> = [];
  lastPage:boolean = true;
  lastEvaluatedKey:{};
  isBusy:boolean = false;

  result: any;
  queryStatement: {};

  constructor(private router: Router, private _productDatabase: ProductDatabase) {
    console.log("ProductListComponent constructor");
    
    this.queryStatement = {
      limit: 10,
      indexName: "ProductStatusIndex",
      productStatus: "SUBMITTED"
    };
  }

  ngOnInit() {
    this.getItemList();
  }

  clickNewButton() {
    this.router.navigate(["/products/new"]);
  }

  onScrollDown() {
    if (this.lastPage || this.isBusy) {
      console.log(this.lastPage);
      console.log(this.isBusy);
      return ;
    }
    this.queryStatement["lastEvaluatedKey"] = {
      createdTimestamp: this.lastEvaluatedKey["createdTimestamp"],
      id: this.lastEvaluatedKey["id"],
      productStatus: this.lastEvaluatedKey["productStatus"]
    };

    this.getItemList();
  }

  private getItemList() {
    this.isBusy = true;
    this._productDatabase.getItemList(this.queryStatement)
    .then(result => {
      console.log(result);
      if ("LastEvaluatedKey" in result) {
        this.lastEvaluatedKey = result.LastEvaluatedKey;
        this.lastPage = false;
      } else {
        this.lastPage = true;
      }

      result.Items.forEach(item => {
        this.items.push(item);
        this.isBusy = false;
      });
    });
  }
}