import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";

import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';

import {
  ProductDatabase,
  ProductGetItemArgument
} from "./product.service";
import { Product } from "./product";


@Component({
  selector: "product-retrieve",
  host: {
    class: ""
  },
  templateUrl: "./product-retrieve.component.html",
  styleUrls: ["./product-retrieve.component.css"]
})
export class ProductRetrieveComponent implements OnInit {
  private _productDetail: any;
  closeResult: string;

  constructor(private router: Router,
              private activatedRoute: ActivatedRoute,
              private modalService: NgbModal,
              private _productDatabase: ProductDatabase) {
    console.log("ProductRetrieveComponent");
    
  }

  ngOnInit() {
    this.activatedRoute.params.subscribe(
      (param: any) => {
        let productId = param["productId"];
        let productGetItemArgument = new ProductGetItemArgument(productId);
        this._productDetail = this._productDatabase.getItem(productGetItemArgument);
      }
    );
  }

  openOptionsModal(content) {
    this.modalService.open(content).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  logout() {
    this.router.navigate(["/logout"]);
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }
}