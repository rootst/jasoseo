import { Injectable } from "@angular/core";
import { Http, Response, Headers, RequestOptions  } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import {
  PaginationMixin,
  DynamoDBPaginationResult
} from "@room36/database/mixin/pagination";
import {
  DynamoDBCreateItemArgument,
  DynamoDBGetItemArgument
} from "@room36/database/nosql/dynamoDB";

import { environment } from "../../environments/environment";
import {
  JasoseoDynamoDBEnvironment,
  JasoseoDynamoDB
} from "../shared/jasoseo";
import {
  Product
} from "./product";


@Injectable()
export class ProductCreateItemArgument extends DynamoDBCreateItemArgument {
  constructor() {
    super();
  }

  getAttribute(valueName: string): any {
    // FIXME: validate this code works or not.
    /* is this code works?
    if (valueName in this) {
      return this[valueName];
    }
    */
    return "undefined";
  }
}


@Injectable()
export class ProductGetItemArgument extends DynamoDBGetItemArgument {
  private _id: string;

  constructor(id: string) {
    super();

    this._id = id;
  }

  getAttribute(valueName: string): any {
    if (valueName === "id") {
      return this._id;
    }
    return "undefined";
  }
}


@Injectable()
export class ProductAuthenticatorEnvironment extends JasoseoDynamoDBEnvironment {
  constructor() {
    super();
  }

  getAttribute(valueName: string): any {
    let returnValue = super.getAttribute(valueName);

    if (returnValue === "undefined") {

    }
    return returnValue;
  }
}


@Injectable()
export class ProductDatabase extends JasoseoDynamoDB implements PaginationMixin {
  constructor(private _productAuthenticatorEnvironment: ProductAuthenticatorEnvironment) {
    super("Product", _productAuthenticatorEnvironment);
  }

  private generateHash(): Promise<string> {
    let promise = new Promise((resolve, reject) => {
      const ALPHABET = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
      const ID_LENGTH = 10;
      let rtn = "";
      for (let i = 0; i < ID_LENGTH; i++) {
        rtn += ALPHABET.charAt(Math.floor(Math.random() * ALPHABET.length));
      }
      return resolve(rtn);
    });
    return promise;
  }

  private validateId(generatedId: string): Promise<any> {
    let promise = new Promise((resolve, reject) => {
      let parameters = {
        TableName: "Product",
        ProductionExpression: "id"
      };
      this._documentClient.scan(parameters, (errorResult, result) => {
        if (errorResult) {
          errorResult._status = 500;
          return reject(errorResult);
        }
        result.Items.forEach((product) => {
          if (generatedId === product.id) {
            let errorResult = {};
            errorResult["_status"] = 500;
            return reject(errorResult);
          }
        });
        return resolve(generatedId);
      });
    });
    return promise;
  }

  private preprocessCreation(productCreateItemArgument: ProductCreateItemArgument): Promise<any> {
    let promise = new Promise((resolve, reject) => {
      let ownerId = productCreateItemArgument.getAttribute("ownerId");
      let createdTimestamp = productCreateItemArgument.getAttribute("createdTimestamp");
      let productStatus = productCreateItemArgument.getAttribute("productStatus");
      let title = productCreateItemArgument.getAttribute("title");
      let expirationDate = productCreateItemArgument.getAttribute("expirationDate");
      let budget = productCreateItemArgument.getAttribute("budget");
      let content = productCreateItemArgument.getAttribute("content");
      let recruitingExpirationTimestamp = productCreateItemArgument.getAttribute("recruitingExpirationTimestamp");
      let beginningTimestamp = productCreateItemArgument.getAttribute("beginningTimestamp");
      let relatedFiles = productCreateItemArgument.getAttribute("relatedFiles");
      let relatedSkills = productCreateItemArgument.getAttribute("relatedSkills");

      if ((ownerId === "undefined") ||
          (createdTimestamp === "undefined") ||
          (productStatus === "undefined") ||
          (title === "undefined") ||
          (expirationDate === "undefined") ||
          (budget === "undefined") ||
          (content === "undefined") ||
          (recruitingExpirationTimestamp === "undefined") ||
          (beginningTimestamp === "undefined") ||
          (relatedFiles === "undefined") ||
          (relatedSkills === "undefined")) {
        return reject();
      }

      this.generateHash()
      .then(result => this.validateId(result), errorResult => reject(errorResult))
      .then((result) => {
        let dynamoDBQueryParameter = {};
        dynamoDBQueryParameter["Item"] = {
          id: result,
          ownerId: ownerId,
          createdTimestamp: createdTimestamp,
          productStatus: productStatus,
          title: title,
          expirationDate: expirationDate,
          budget: budget,
          planContent: content,
          recruitingExpirationDate: recruitingExpirationTimestamp,
          beginningDate: beginningTimestamp,

          planRelatedFiles: relatedFiles,
          relatedSkills: relatedSkills,
        }
        return resolve(dynamoDBQueryParameter);

      }, (errorResult) => {
        return reject(errorResult);
      })
      .catch((errorResult) => {
        return reject(errorResult);
      });
    });
    return promise;
  }

  private processCreation(queryParameter): Promise<any> {
    let promise = new Promise((resolve, reject) => {
      this._documentClient.put(queryParameter, (errorResult, result) => {
        if (errorResult) {
          errorResult._status = 500;
          return reject(errorResult);
        }

        return resolve(result);
      });
    });
    return promise;
  };

  private postProcessCreation(processedData) {
    let promise = new Promise((resolve, reject) => {
      return resolve(processedData);
    });
    return promise;
  }

  createItem(productCreateItemArgument: ProductCreateItemArgument): Promise<Product> {
    let promise = new Promise((resolve, reject) => {
      console.log("product: " + JSON.stringify(productCreateItemArgument));
      this.preprocessCreation(productCreateItemArgument)
      .then(result => this.processCreation(result), errorResult => reject(errorResult))
      .then(result => this.postProcessCreation(result), errorResult => reject(errorResult))
      .then(result => resolve(result), errorResult => reject(errorResult))
      .catch((errorResult) => {
        throw(errorResult);
      });
    });
    return promise;
  }

  private preprocessGet(productGetItemArgument: ProductGetItemArgument): Promise<any> {
    let promise = new Promise((resolve, reject) => {
      let productId = productGetItemArgument.getAttribute("id");
      
      if (productId === "undefined") {
        console.warn("productId is NaN " + productId);
        return reject();
      }
      let dynamoDBQueryParameter = {
        TableName: "Product",
        KeyConditionExpression: "#id = :id",
        ExpressionAttributeNames: {
          '#id': "id"
        },
        ExpressionAttributeValues: {
          ':id': productId
        }
      };
      return resolve(dynamoDBQueryParameter);
    });
    return promise;
  }

  private processGet(queryParameter): Promise<any> {
    let promise = new Promise((resolve, reject) => {
      this._documentClient.query(queryParameter, (errorResult, result) => {
        if (errorResult) {
          errorResult._status = 500;
          return reject(errorResult);
        }

        return resolve(result);
      });
    });
    return promise;
  };

  private postProcessGet(processedData) {
    let promise = new Promise((resolve, reject) => {
      if ("Items" in processedData) {
        let product = new Product();
        product.id = processedData["Items"][0]["id"];
        product.ownerId = processedData["Items"][0]["ownerId"];
        product.createdTimestamp = processedData["Items"][0]["createdTimestamp"];
        product.productStatus = processedData["Items"][0]["productStatus"];
        product.title = processedData["Items"][0]["title"];
        product.beginningTimestamp = processedData["Items"][0]["beginningDate"];
        product.recruitingExpirationTimestamp = processedData["Items"][0]["recruitingExpirationDate"];
        product.relatedSkills = processedData["Items"][0]["relatedSkills"];
        product.content = processedData["Items"][0]["planContent"];
        product.relatedFiles = processedData["Items"][0]["planRelatedFiles"];
        return resolve(product);
      }
      return reject();
    });
    return promise;
  }

  getItem(productGetItemArgument: ProductGetItemArgument): Promise<Product> {
    let promise = new Promise((resolve, reject) => {
      console.log("product: " + JSON.stringify(productGetItemArgument));
      this.preprocessGet(productGetItemArgument)
      .then(result => this.processGet(result), errorResult => reject(errorResult))
      .then(result => this.postProcessGet(result), errorResult => reject(errorResult))
      .then(result => resolve(result), errorResult => reject(errorResult))
      .catch((errorResult) => {
        throw(errorResult);
      });
    });
    return promise;
  }

  private preprocessGetList(queryStatement: {}): Promise<any> {
    let promise = new Promise((resolve, reject) => {
      let dynamoDBQueryParameter = {
        TableName: "Product",
      };

      if ("indexName" in queryStatement) {
        dynamoDBQueryParameter["IndexName"] = queryStatement["indexName"];
        dynamoDBQueryParameter["ScanIndexForward"] = true;
        dynamoDBQueryParameter["KeyConditionExpression"] = '#productStatus = :productStatus';
        dynamoDBQueryParameter["ExpressionAttributeNames"] = {
          '#productStatus': 'productStatus'
        };
        dynamoDBQueryParameter["ExpressionAttributeValues"] = {
          ':productStatus': queryStatement["productStatus"]
        };
      }

      if ("limit" in queryStatement) {
        dynamoDBQueryParameter["Limit"] = queryStatement["limit"];
      }

      if (("lastEvaluatedKey" in queryStatement)) {
        dynamoDBQueryParameter["ExclusiveStartKey"] = {
          "createdTimestamp": Number(queryStatement["lastEvaluatedKey"]["createdTimestamp"]),
          "id": queryStatement["lastEvaluatedKey"]["id"],
          "productStatus": queryStatement["lastEvaluatedKey"]["productStatus"],
        };
      }

      return resolve(dynamoDBQueryParameter);
    });
    return promise;
  }

  private processGetList(queryParameter): Promise<any> {
    let promise = new Promise((resolve, reject) => {
      this._documentClient.query(queryParameter, (errorResult, result) => {
        if (errorResult) {
          errorResult._status = 500;
          return reject(errorResult);
        }

        return resolve(result);
      });
    });
    return promise;
  };

  private postProcessGetList(processedData) {
    let promise = new Promise((resolve, reject) => {
      return resolve(processedData);
    });
    return promise;
  }

  getItemList(queryStatement: {}): Promise<DynamoDBPaginationResult> {
    let promise = new Promise((resolve, reject) => {
      console.log("product: " + queryStatement);
      this.preprocessGetList(queryStatement)
      .then(result => this.processGetList(result), errorResult => reject(errorResult))
      .then(result => this.postProcessGetList(result), errorResult => reject(errorResult))
      .then(result => resolve(result), errorResult => reject(errorResult))
      .catch((errorResult) => {
        throw(errorResult);
      });
    });
    return promise;
  }
}