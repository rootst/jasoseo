import { Injectable } from "@angular/core";

import { environment } from "../../environments/environment";
import {
  JasoseoDynamoDBModel
} from "../shared/jasoseo";


export class Product extends JasoseoDynamoDBModel {
  public _id: string;
  public _ownerId: string;
  public _createdTimestamp: number;
  public _productStatus: string;

  public _title: string;
  public _expirationTimestamp: number;
  public _budget: number;
  public _content: string;
  public _relatedFiles: string;
  public _relatedSkills: string;
  public _recruitingExpirationTimestamp: number;
  public _beginningTimestamp: number;

  public _commentCount: number;
  public _appliedPartnerCount: number;

  get id(): string {
      return this._id;
  }
  set id(id: string) {
      this._id = id;
  }
  get ownerId(): string {
      return this._ownerId;
  }
  set ownerId(ownerId: string) {
      this._ownerId = ownerId;
  }
  get createdTimestamp(): number {
      return this._createdTimestamp;
  }
  set createdTimestamp(createdTimestamp: number) {
      this._createdTimestamp = createdTimestamp;
  }
  get productStatus(): string {
      return this._productStatus;
  }
  set productStatus(productStatus: string) {
      this._productStatus = productStatus;
  }
  get title(): string {
      return this._title;
  }
  set title(title: string) {
      this._title = title;
  }
  get expirationTimestamp(): number {
      return this._expirationTimestamp;
  }
  set expirationTimestamp(expirationTimestamp: number) {
      this._expirationTimestamp = expirationTimestamp;
  }
  get content(): string {
      return this._content;
  }
  set content(content: string) {
      this._content = content;
  }
  get relatedFiles(): string {
      return this._relatedFiles;
  }
  set relatedFiles(relatedFiles: string) {
      this._relatedFiles = relatedFiles;
  }
  get relatedSkills(): string {
      return this._relatedSkills;
  }
  set relatedSkills(relatedFiles: string) {
      this._relatedSkills = relatedFiles;
  }
  get recruitingExpirationTimestamp(): number {
      return this._recruitingExpirationTimestamp;
  }
  set recruitingExpirationTimestamp(recruitingExpirationTimestamp: number) {
      this._recruitingExpirationTimestamp = recruitingExpirationTimestamp;
  }
  get beginningTimestamp(): number {
      return this._beginningTimestamp;
  }
  set beginningTimestamp(beginningTimestamp: number) {
      this._beginningTimestamp = beginningTimestamp;
  }
  get commentCount(): number {
      return this._commentCount;
  }
  set commentCount(commentCount: number) {
      this._commentCount = commentCount;
  }
  get appliedPartnerCount(): number {
      return this._appliedPartnerCount;
  }
  set appliedPartnerCount(appliedPartnerCount: number) {
      this._appliedPartnerCount = appliedPartnerCount;
  }

  constructor(initialValue?: {
    _id?: string,
    _ownerId?: string,
    _createdTimestamp?: number,
    _productStatus?: string,
    _title?: string,
    _expirationTimestamp?: number,
    _budget?: number,
    _content?: string,
    _relatedFiles?: string,
    _relatedSkills?: string,
    _recruitingExpirationTimestamp?: number,
    _beginningTimestamp?: number,
    _commentCount?: number,
    _appliedPartnerCount?: number
  }) {
    super();

    if (initialValue) {
        this._id = initialValue._id;
        this._ownerId = initialValue._ownerId;
        this._createdTimestamp = initialValue._createdTimestamp;
        this._productStatus = initialValue._productStatus;
        this._title = initialValue._title;
        this._expirationTimestamp = initialValue._expirationTimestamp;
        this._budget = initialValue._budget;
        this._content = initialValue._content;
        this._relatedFiles = initialValue._relatedFiles;
        this._relatedSkills = initialValue._relatedSkills;
        this._recruitingExpirationTimestamp = initialValue._recruitingExpirationTimestamp;
        this._beginningTimestamp = initialValue._beginningTimestamp;
        this._commentCount = initialValue._commentCount;
        this._appliedPartnerCount = initialValue._appliedPartnerCount;
    }
  }
}