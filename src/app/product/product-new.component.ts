import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";

import { Product } from "./product";
import {
  ProductDatabase,
  ProductCreateItemArgument
} from "./product.service";

import { UserDatabase } from "../user/user.service";


/**
 * This component is responsible for displaying and controlling
 * the registration of the user.
 */
@Component({
  selector: "product-new",
  templateUrl: "./product-new.component.html",
  styleUrls: ["./product-new.component.css"]
})
export class ProductNewComponent implements OnInit {
  public recruitingExpirationDateString: string;
  public beginningDateString: string;

  _product: Product;
  errorMessage: string;

  constructor(private router: Router, private _productDatabase: ProductDatabase, private _userDatabase: UserDatabase) {
    console.log("ProductNewComponent");
  }

  ngOnInit() {
    this._product = new Product();
  }

  onRegister() {
    this.errorMessage = null;

    this._product.ownerId = this._userDatabase.getUserPool().getCurrentUser().username;
    this._product.productStatus = "SUBMITTED";
    this._product.recruitingExpirationTimestamp = new Date(this.recruitingExpirationDateString).getTime();
    this._product.beginningTimestamp = new Date(this.beginningDateString).getTime();
    this._product.createdTimestamp = new Date().getTime();

    let productCreateItemArgument = new ProductCreateItemArgument();
    this._productDatabase.createItem(productCreateItemArgument);
  }
}