﻿import { Injectable } from "@angular/core";
import {
  AmazonWebService,
  AmazonWebServiceEnvironment
} from "@room36/platform/amazonWebService";
import {
  DynamoDBService,
  DynamoDBModel,
  DynamoDBAuthenticatorEnvironment,
  DynamoDBCreateItemArgument,
  DynamoDBGetItemArgument
} from "@room36/database/nosql/dynamoDB";
import { S3Service, S3ResourceAccessorEnvironment } from "@room36/resource/s3";

import { environment } from "../../environments/environment";


@Injectable()
export class JasoseoPlatformEnvironment extends AmazonWebServiceEnvironment {
  constructor() {
    super(environment.cognito.region, environment.cognito.identityPoolId, environment.cognito.userPoolId, environment.cognito.clientId);
  }
}


@Injectable()
export class JasoseoAmazonWebService extends AmazonWebService {
  constructor(private _environment: JasoseoPlatformEnvironment) {
    super(_environment);
  }
}


@Injectable()
export abstract class JasoseoDynamoDBEnvironment extends DynamoDBAuthenticatorEnvironment {
  constructor() {
    super(environment.cognito.region, environment.cognito.identityPoolId, environment.cognito.userPoolId, environment.cognito.clientId);
  }

  getAttribute(valueName: string): any {
    let returnValue = super.getAttribute(valueName);

    if (returnValue === "undefined") {

    }
    return returnValue;
  }
}


@Injectable()
export abstract class JasoseoDynamoDBModel extends DynamoDBModel {
  constructor() {
    super();
  }
}


@Injectable()
export abstract class JasoseoDynamoDB extends DynamoDBService {
  constructor(_tableName: string, private _environment: JasoseoDynamoDBEnvironment) {
    super(_tableName, _environment);
  }

  abstract createItem(dynamoDBCreateItemArgument: DynamoDBCreateItemArgument): Promise<JasoseoDynamoDBModel>;
  abstract getItem(dynamoDBGetItemArgument: DynamoDBGetItemArgument): Promise<JasoseoDynamoDBModel>;
}


@Injectable()
export class JasoseoS3Environment extends S3ResourceAccessorEnvironment {
  constructor() {
    super(environment.cognito.region, environment.cognito.identityPoolId, environment.cognito.userPoolId, environment.cognito.clientId);
  }
}


@Injectable()
export class JasoseoS3 extends S3Service {
  constructor(private _environment: JasoseoS3Environment) {
    super(_environment);
  }
}