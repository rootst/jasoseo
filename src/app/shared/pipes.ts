import { Pipe, PipeTransform } from "@angular/core";


@Pipe({
  name: 'cognitoAttributePipe'
})
export class CustomPipe implements PipeTransform {
  transform(obj: any, args: string, args1: string): string {
    let returnString = "undefined";

    if (obj) {
      let userAttributes: Array<any> = obj[args];
      userAttributes.forEach((attribute) => {
        if (attribute["Name"] === args1) {
          console.log(attribute["Value"]);
          returnString = attribute["Value"];
        }
      });
    }
    return returnString;
  }
}

@Pipe({
  name: 'dynamoDBAttributePipe'
})
export class DynamoDBAttributePipe implements PipeTransform {
  transform(obj: any, args: string): string {
    let returnString = "undefined";

    if (obj) {
      if (args in obj) {
        returnString = obj[args];
        console.log(returnString);
      }
    }
    return returnString;
  }
}