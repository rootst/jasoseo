﻿import { Injectable } from "@angular/core";
import {
  CanActivate,
  CanActivateChild,
  Router,
  ActivatedRouteSnapshot,
  RouterStateSnapshot
} from "@angular/router";

import { UserDatabase } from "../user/user.service";


@Injectable()
export class AuthenticationGuard implements CanActivate, CanActivateChild {
  constructor(private _userDatabase: UserDatabase, private router: Router) {

  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<boolean> {
    let url: string = state.url;

    //return this.checkLogin(url);
    return this._userDatabase.isAuthenticated();
  }

  canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<boolean> {
    return this.canActivate(route, state);
  }

  checkLogin(url: string): Promise<boolean> {
    let promise = new Promise((resolve, reject) => {
      this._userDatabase.isAuthenticated()
      .then((result) => {
        return resolve(true);
      }, (errorResult) => {
        // Store the attempted URL for redirecting
        //this.userLoginService.redirectUrl = url;

        // Navigate to the login page with extras
        this.router.navigate(["/"]);
        return reject(false);
      })
      .catch((errorResult) => {
        // Store the attempted URL for redirecting
        //this.userLoginService.redirectUrl = url;

        // Navigate to the login page with extras
        this.router.navigate(["/"]);
        return reject(false);
      });
    });
    return promise;
  }
}