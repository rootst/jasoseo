﻿import { Injectable } from "@angular/core";


export class User {
  private _name: string;
  private _email: string;
  private _password: string;

  get name(): string {
    return this._name;
  }
  set name(name: string) {
    this._name = name;
  }
  get email(): string {
    return this._email;
  }
  set email(email: string) {
    this._email = email;
  }
  get password(): string {
    return this._password;
  }
  set password(password: string) {
    this._password= password;
  }

  constructor(initialValue?: {
    name?: string;
    email?: string;
    password?: string;
  }) {
    if (initialValue) {
      this._name = initialValue.name;
      this._email= initialValue.email;
      this._password = initialValue.password;
    }
  }
}
