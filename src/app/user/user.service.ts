﻿import { Injectable, Inject } from "@angular/core";
import {
  CognitoService,
  CognitoCreateItemArgument,
  CognitoGetItemArgument,
  CognitoAuthenticatorEnvironment,
  CognitoConfirmRegistrationArgument,
  CognitoResendConfirmRegistrationArgument,
  CognitoLoginArgument,
  CognitoForgotPasswordArgument,
  CognitoConfirmPasswordArgument
} from "@room36/authentication/cognito";

import { environment } from "../../environments/environment";


@Injectable()
export class UserCreateItemArgument extends CognitoCreateItemArgument {
  constructor(username: string, password: string) {
    super(username, password);
  }
}


@Injectable()
export class UserGetItemArgument extends CognitoGetItemArgument {
  constructor(username: string) {
    super(username);
  }
}


@Injectable()
export class UserAuthenticatorEnvironment extends CognitoAuthenticatorEnvironment {
  constructor() {
    let region: string = environment.cognito.region;
    let identityPoolId: string = environment.cognito.identityPoolId;
    let userPoolId: string = environment.cognito.userPoolId;
    let clientId: string = environment.cognito.clientId;

    super(region, identityPoolId, userPoolId, clientId);
  }
}


@Injectable()
export class UserConfirmRegistrationArgument extends CognitoConfirmRegistrationArgument {
  constructor(username: string, confirmationCode: string) {
    super(username, confirmationCode);
  }
}


@Injectable()
export class UserResendConfirmRegistrationArgument extends CognitoResendConfirmRegistrationArgument {
  constructor(username: string) {
    super(username);
  }
}


@Injectable()
export class UserLoginArgument extends CognitoLoginArgument {
  constructor(username: string, password: string) {
    super(username, password);
  }
}


@Injectable()
export class UserForgotPasswordArgument extends CognitoForgotPasswordArgument {
  constructor(username: string) {
    super(username);
  }
}


@Injectable()
export class UserConfirmPasswordArgument extends CognitoConfirmPasswordArgument {
  constructor(username: string, password: string, verificationCode: string) {
    super(username, password, verificationCode);
  }
}


@Injectable()
export class UserDatabase extends CognitoService {
  constructor(private userAuthenticatorEnvironment: UserAuthenticatorEnvironment) {
    super(userAuthenticatorEnvironment);
  }
}