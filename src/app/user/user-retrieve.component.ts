import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';

import {
  UserDatabase,
  UserGetItemArgument
} from "./user.service";


@Component({
  selector: "user-retrieve",
  host: {
    class: ""
  },
  templateUrl: "./user-retrieve.component.html",
  styleUrls: ["./user-retrieve.component.css"]
})
export class UserRetrieveComponent implements OnInit {
  private _userDetail: any;
  private closeResult: string;

  constructor(private router: Router, private activatedRoute: ActivatedRoute, private modalService: NgbModal, private _userDatabase: UserDatabase) {
    console.log("UserRetrieveComponent");
    
  }

  ngOnInit() {
    this.activatedRoute.params.subscribe(
      (param:any) => {
        let username = param['userId'];
        console.log(username);
        let userGetItemArgument = new UserGetItemArgument(username);
        this._userDetail = this._userDatabase.getItem(userGetItemArgument);
      }
    );
  }

  openOptionsModal(content) {
    this.modalService.open(content).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  onLogout() {
    this._userDatabase.getUserPool().getCurrentUser().signOut();
    this.router.navigate(["/"]);
  }

  avatarUpload(files) {
    console.log(files);
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }
}