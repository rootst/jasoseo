import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";

import {
  JasoseoAmazonWebService,
  JasoseoDynamoDB,
  JasoseoS3
} from "./shared/jasoseo";
import { UserDatabase } from "./user/user.service";


@Component({
  selector: "jasoseo",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent implements OnInit {
  constructor(private router: Router,
              private _jasoseoAmazonWebService: JasoseoAmazonWebService,
              private _jasoseoS3: JasoseoS3,
              private _userDatabase: UserDatabase) {
    console.log("AppComponent constructor");

    this._jasoseoAmazonWebService.initializePlatform()
    .then((result) => {
      return this._userDatabase.initializePlugin();
    }, (errorResult) => {
      console.error("Initialize cloud server error: ");
      console.error(errorResult);
    })
    .then((result) => {
      return this._jasoseoS3.initializePlugin();
    }, (errorResult) => {
      console.error("Initialize cognito plugin: ");
      console.error(errorResult);
    })
    .catch((errorResult) => {
      console.error("CRITICAL THROWED");
      console.error(errorResult);
    });
  }

  ngOnInit() {

  }
}