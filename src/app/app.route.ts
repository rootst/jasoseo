import { RouterModule, Routes } from "@angular/router";
import { NgModule } from "@angular/core";

import { AppComponent } from "./app.component";
import { AuthenticationGuard } from "./shared/authenticationGuard";
import { JasoseoWrapperComponent } from "./public/jasoseoWrapper.component";
import { HomeComponent } from "./public/home.component";

import { ProjectNewComponent } from "./product/product-new.component";
import { ProjectRetrieveComponent } from "./product/product-retrieve.component";

import { UserRetrieveComponent } from "./user/user-retrieve.component";


/*
const productRoute: Routes = [
  {
    path: 'product',
    redirectTo: '',
    pathMatch: 'full',
    canActivate: [AuthenticationGuard],
    children: [
      { path: 'new', component: ProjectNewComponent },
    ]
  }
]
*/


const appRoute: Routes = [
  {
    path: '',
    component: JasoseoWrapperComponent,
    //canActivate: [AuthenticationGuard],
    children: [
      //...productRoute,
      { path: '', component: HomeComponent },
      { path: 'products/new', component: ProjectNewComponent },
      { path: 'products/:productId', component: ProjectRetrieveComponent },
      { path: 'users/:userId', component: UserRetrieveComponent}
    ]
  }
]


@NgModule({
  imports: [
    RouterModule.forRoot(appRoute)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoute { }