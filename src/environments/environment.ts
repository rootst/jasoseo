// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `angular-cli.json`.

export const environment = {
  production: false,
  appId: {
    mobileAnalytics: "208477de09ce43c7b19caebc75f04378"
  },
  cognito: {
    region: "ap-northeast-2",
    identityPoolId: "ap-northeast-2:bf2a50ca-6cc2-40de-9c65-90fbc65c6a04",
    userPoolId: "ap-northeast-2_jMk8Dp2AI",
    clientId: "52ssc2qja9vgf8k7ok0r4mcklg"
  },
};
