"use strict";

const doc = require("dynamodb-doc");
const dynamo = new doc.DynamoDB();

module.exports.handler = (event, context, callback) => {
  const postData = event.postData;
  const pathParameter = event.pathParameter;
  const queryString = event.queryString;
  let dynamoDBQueryParameter = {
    TableName: "Project"
  };

  function generateHash() {
    let promise = new Promise((resolve, reject) => {
      let ALPHABET = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
      let ID_LENGTH = 10;
      let rtn = "";
      for (let i = 0; i < ID_LENGTH; i++) {
        rtn += ALPHABET.charAt(Math.floor(Math.random() * ALPHABET.length));
      }
      return resolve(rtn);
    });
    return promise;
  }
    
  function validateId(generatedId) {
    let promise = new Promise((resolve, reject) => {
      let parameters = {
        TableName: "Project",
        ProjectionExpression: "id"
      };
      dynamo.scan(parameters, (errorResult, result) => {
        if (errorResult) {
          errorResult._status = 500;
          return reject(errorResult);
        }
        result.Items.forEach((project) => {
          if (generatedId === project.id) {
            let errorResult = {};
            errorResult._status = 500;
            return reject(errorResult);
          }
        });
        return resolve(generatedId);
      });
    });
    return promise;
  }

  function preprocess() {
    let promise =new Promise((resolve, reject) => {
      if ("indexName" in queryString) {
        dynamoDBQueryParameter.IndexName = queryString.indexName;
        dynamoDBQueryParameter.ScanIndexForward = true;
        dynamoDBQueryParameter.KeyConditionExpression = '#projectStatus = :projectStatus',
        dynamoDBQueryParameter.ExpressionAttributeNames = {
          '#projectStatus': 'projectStatus'
        },
        dynamoDBQueryParameter.queryString = {
          ':projectStatus': queryString.projectStatus
        }
      }

      if ("limit" in queryString) {
        dynamoDBQueryParameter.Limit = queryString.limit;
      }

      if (("lastEvaluatedKey1" in queryString) && ("lastEvaluatedKey2" in queryString) && ("lastEvaluatedKey3" in queryString)) {
        dynamoDBQueryParameter.ExclusiveStartKey = {
          "createdTimestamp": Number(queryString.lastEvaluatedKey1),
          "id": queryString.lastEvaluatedKey2,
          "projectStatus": queryString.lastEvaluatedKey3,
        };
      }

      if (!(("ownerId" in postData) && ("createdTimestamp" in postData) && ("projectStatus" in postData) && ("title" in postData) &&
            ("expirationDate" in postData) && ("budget" in postData) && ("planStatus" in postData) && ("planContent" in postData) &&
            ("meetingType" in postData) && ("managingExpierence" in postData) && ("recruitingExpirationDate" in postData) && ("beginningDate" in postData))) {
        return reject();
      }

      generateHash()
        .then(result => validateId(result), errorResult => reject(errorResult))
        .then(result => {
          dynamoDBQueryParameter.Item = {
            id: result,
            ownerId: postData.ownerId,
            createdTimestamp: postData.createdTimestamp,
            projectStatus: postData.projectStatus,
            title: postData.title,
            expirationDate: postData.expirationDate,
            budget: postData.budget,
            planStatus: postData.planStatus,
            planContent: postData.planContent,
            meetingType: postData.meetingType,
            managingExpierence: postData.managingExpierence,
            recruitingExpirationDate: postData.recruitingExpirationDate,
            beginningDate: postData.beginningDate,

            planRelatedFiles: postData.planRelatedFiles,
            relatedSkills: postData.relatedSkills,
            preferringPartner: postData.preferringPartner
          }
          return resolve();

        }, errorResult => reject(errorResult))
        .catch(errorResult => reject(errorResult));
    });
    return promise;
  }

  function process() {
    let promise = new Promise((resolve, reject) => {
      dynamo.putItem(dynamoDBQueryParameter, (errorResult, result) => {
        if (errorResult) {
          errorResult._status = 500;
          return reject(errorResult);
        }

        return resolve(result);
      });
    });
    return promise;
  };

  function postProcess(processedData) {
    let promise = new Promise((resolve, reject) => {
      return resolve(processedData);
    });
    return promise;
  }

  preprocess()
    .then(() => process(), errorResult => callback(errorResult))
    .then(result => postProcess(result), errorResult => callback(errorResult))
    .then(result => callback(null, result), errorResult => callback(errorResult))
    .catch(errorResult => callback(errorResult));
};