// API GATEWAY setting으로 Method Request에 queryParameter 등록
// API GATEWAY Integration Request에 lambda에 넘길 형식 등록 (json참조)

// Provided key element does not match the schema
// => 스키마 형식이 안맞을때 나옴. (Number, String, etc..)
// Key 자체가 이상할 땐 invalid key 어쩌구
"use strict";

const doc = require("dynamodb-doc");
const dynamo = new doc.DynamoDB();

module.exports.handler = (event, context, callback) => {
  const postData = event.postData;
  const pathParameter = event.pathParameter;
  const queryString = event.queryString;
  let dynamoDBQueryParameter = {
    TableName: "Project"
  };

  function preprocess() {
    let promise = new Promise((resolve, reject) => {
      if ("indexName" in queryString) {
        dynamoDBQueryParameter.IndexName = queryString.indexName;
        dynamoDBQueryParameter.ScanIndexForward = true;
        dynamoDBQueryParameter.KeyConditionExpression = '#projectStatus = :projectStatus';
        dynamoDBQueryParameter.ExpressionAttributeNames = {
          '#projectStatus': 'projectStatus'
        };
        dynamoDBQueryParameter.ExpressionAttributeValues = {
          ':projectStatus': queryString.projectStatus
        };
      }

      if ("limit" in queryString) {
        dynamoDBQueryParameter.Limit = queryString.limit;
      }

      if (("lastEvaluatedKey1" in queryString) && ("lastEvaluatedKey2" in queryString) && ("lastEvaluatedKey3" in queryString)) {
        dynamoDBQueryParameter.ExclusiveStartKey = {
          "createdTimestamp": Number(queryString.lastEvaluatedKey1),
          "id": queryString.lastEvaluatedKey2,
          "projectStatus": queryString.lastEvaluatedKey3,
        };
      }

      return resolve();
    });
    return promise;
  }

  function process() {
    let promise = new Promise((resolve, reject) => {
      dynamo.query(dynamoDBQueryParameter, (errorResult, result) => {
        if (errorResult) {
          errorResult._status = 500;
          return reject(errorResult);
        }

        return resolve(result);
      });
    });
    return promise;
  };

  function postProcess(processedData) {
    let promise = new Promise((resolve, reject) => {
      return resolve(processedData);
    });
    return promise;
  }

  preprocess()
    .then(() => process(), errorResult => callback(errorResult))
    .then(result => postProcess(result), errorResult => callback(errorResult))
    .then(result => callback(null, result), errorResult => callback(errorResult))
    .catch(errorResult => callback(errorResult));
};