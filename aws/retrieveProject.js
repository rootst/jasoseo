"use strict";

const doc = require("dynamodb-doc");
const dynamo = new doc.DynamoDB();

module.exports.handler = (event, context, callback) => {
  const postData = event.postData;
  const pathParameter = event.pathParameter;
  const queryString = event.queryString;
  let dynamoDBQueryParameter = {
    TableName: "Project"
  };

  function preprocess() {
    let promise = new Promise((resolve, reject) => {
      dynamoDBQueryParameter.KeyConditionExpression = '#id = :id';
      dynamoDBQueryParameter.ExpressionAttributeNames = {
        '#id': 'id'
      };
      dynamoDBQueryParameter.ExpressionAttributeValues = {
        ':id': pathParameter.projectId
      };

      return resolve();
    });
    return promise;
  }

  function process() {
    let promise = new Promise((resolve, reject) => {
      dynamo.query(dynamoDBQueryParameter, (errorResult, result) => {
        if (errorResult) {
          errorResult._status = 500;
          return reject(errorResult);
        }

        return resolve(result);
      });
    });
    return promise;
  };

  function postProcess(processedData) {
    let promise = new Promise((resolve, reject) => {
      return resolve(processedData);
    });
    return promise;
  }

  preprocess()
    .then(() => process(), errorResult => callback(errorResult))
    .then(result => postProcess(result), errorResult => callback(errorResult))
    .then(result => callback(null, result), errorResult => callback(errorResult))
    .catch(errorResult => callback(errorResult));
};