"use strict";

const doc = require("dynamodb-doc");
const dynamo = new doc.DynamoDB();

module.exports.handler = (event, context, callback) => {
  const postData = event.postData;
  const pathParameter = event.pathParameter;
  const queryString = event.queryString;
  let dynamoDBQueryParameter = {
    TableName: "Comment"
  };

  function preprocess() {
    let promise =new Promise((resolve, reject) => {
      if ("indexName" in queryString) {
        dynamoDBQueryParameter.IndexName = queryString.indexName;
        dynamoDBQueryParameter.ScanIndexForward = true;

        if ("ownerId" in queryString) {
          dynamoDBQueryParameter.KeyConditionExpression = '#ownerId = :ownerId',
          dynamoDBQueryParameter.ExpressionAttributeNames = {
            '#ownerId': 'ownerId'
          },
          dynamoDBQueryParameter.ExpressionAttributeValues = {
            ':ownerId': queryString.ownerId
          }
        }
      }

      if ("limit" in queryString) {
        dynamoDBQueryParameter.Limit = queryString.limit;
      }
  
      if (("lastEvaluatedKey1" in queryString) && ("lastEvaluatedKey2" in queryString)) {
        dynamoDBQueryParameter.ExclusiveStartKey = {
          "createdTimestamp": Number(queryString.lastEvaluatedKey1),
          "id": queryString.lastEvaluatedKey2
        };
      }

      if ("projectId" in pathParameter) {
        dynamoDBQueryParameter.KeyConditionExpression = '#projectId = :projectId',
          dynamoDBQueryParameter.ExpressionAttributeNames = {
            '#projectId': 'projectId'
          },
          dynamoDBQueryParameter.ExpressionAttributeValues = {
            ':projectId': pathParameter.projectId
          }
      }

      return resolve();
    });
    return promise;
  }

  function process() {
    let promise = new Promise((resolve, reject) => {
      dynamo.query(dynamoDBQueryParameter, (errorResult, result) => {
        if (errorResult) {
          errorResult._status = 500;
          return reject(errorResult);
        }

        return resolve(result);
      });
    });
    return promise;
  };

  function postProcess(processedData) {
    let promise = new Promise((resolve, reject) => {
      return resolve(processedData);
    });
    return promise;
  }

  preprocess()
    .then(() => process(), errorResult => callback(errorResult))
    .then(result => postProcess(result), errorResult => callback(errorResult))
    .then(result => callback(null, result), errorResult => callback(errorResult))
    .catch(errorResult => callback(errorResult));
};